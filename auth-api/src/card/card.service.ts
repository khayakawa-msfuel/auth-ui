import { Injectable } from '@nestjs/common';
import { Repository, getRepository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Card } from './card.entity';
import { resolve } from 'dns';

@Injectable()
export class CardService {
    constructor(
        @InjectRepository(Card)
        private cardRepository: Repository<Card>,
    ) { }

    async findByCustNo(card_no): Promise<Card[]> {
        let custNo = Number(card_no);
        return await getRepository(Card)
            .createQueryBuilder('card')
            .where("cust_no=:cust_no and status=0 and track_2 is not null and length(track_2)>1", {cust_no: custNo})
            .getMany();
    }
}