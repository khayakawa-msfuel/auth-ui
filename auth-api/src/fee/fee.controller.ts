import { Controller, Get } from '@nestjs/common';
import { FeeService } from './fee.service'

@Controller('fee')
export class FeeController {
  constructor(private feeService: FeeService){}

  @Get('description')
  GetDesc(): Promise<string[]> {
    return this.feeService.getDescriptions()
  }

  @Get('category')
  GetCategory(): Promise<string[]> {
    return this.feeService.getCategories()
  }
}
