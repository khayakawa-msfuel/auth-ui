import { Module } from '@nestjs/common';
import { PromptService } from './prompt.service';
import { PromptController } from './prompt.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Prompt } from './prompt.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Prompt]),
  ],
  providers: [PromptService],
  controllers: [PromptController],
  exports: [PromptService],
})
export class PromptModule {}
