import { Controller, Get, Param } from '@nestjs/common';
import { Merc } from '../merc.entity';
import { MercService } from '../merc.service';


@Controller('merc')
export class MercController {
    constructor(private mercService: MercService){}

    @Get()
    index(): Promise<Merc[]> {
        return this.mercService.findAll();
    }

    @Get('no/:merc_no')
    getMerc(@Param('merc_no') merc_no): Promise<Merc> {
        return this.mercService.find(merc_no);
    }

    @Get('type/:type')
    getMercByType(@Param('type') merc_type): Promise<Merc[]> {
        return this.mercService.findByType(merc_type);
    }
}
