import { Test, TestingModule } from '@nestjs/testing';
import { MercService } from './merc.service';

describe('MercService', () => {
  let service: MercService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MercService],
    }).compile();

    service = module.get<MercService>(MercService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
