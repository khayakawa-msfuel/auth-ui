# Getting Started
Create `auth-api/.env` file.  The file should look like this:
```
DB_HOST=msfleet.dev
DB_USER=myname[msfleet]
DB_PASS=my_dev_db_pw
```
Once that file is created, follow the README instructions in each project: [`auth-api`](auth-api/README.md) (use the dev/watch mode) and [`auth-ui`](auth-ui/README.md).
